$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            // $(".navbar-light").addClass("fixed-top");  
            $(".sticky-top").addClass("darkHeader");
        } else {
            // $(".navbar-light").removeClass("fixed-top");
            $(".sticky-top").removeClass("darkHeader");
        }
    });

    $('.navbar-nav li a, .navbar-brand a').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        href = '#' + href.split('#').pop();

        var $target = $(href).offset().top - 140;

        $('html, body').animate({
            'scrollTop': $target
        }, 900, 'swing', function() {
            window.history.pushState("object or string", "Title", href);
        });
        if (screen.width <= 991) {
            $(".navbar-toggler").trigger('click');
        }
    });

    new WOW().init();

    AOS.init({
        duration: 2000,
        delay: 0.5,
    })


    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scroll').fadeIn();
        } else {
            $('.scroll').fadeOut();
        }
    });
    $('.box').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    $('#scroll1').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });


    $('.bb').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    
    $('#awards').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        navigation: true,
        margin: 20,
        ltr: false,
        loop: true,
        dots: true,
        nav: false,
        // navText: ["<img src='owlCarousel/img/left.png'>","<img src='owlCarousel/img/right.png'>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    $('#tie-ups').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        navigation: true,
        margin: 20,
        ltr: false,
        loop: true,
        dots: true,
        nav: false,
        // navText: ["<img src='owlCarousel/img/left.png'>","<img src='owlCarousel/img/right.png'>"],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
    $('#Testimonial_slide').owlCarousel({
        autoplay: false,
        autoplayTimeout: 100000,
        navigation: true,
        margin: 5,
        ltr: false,
        loop: true,
        dots: true,
        nav: false,
        // navText: ["<img src='owlCarousel/img/left.png'>","<img src='owlCarousel/img/right.png'>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });
    $('#career_opport_slide').owlCarousel({
        autoplay: false,
        autoplayTimeout: 100000,
        navigation: true,
        margin: 30,
        ltr: false,
        loop: true,
        dots: true,
        nav: false,
        // navText: ["<img src='owlCarousel/img/left.png'>","<img src='owlCarousel/img/right.png'>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    $('#recruiters_slide').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        navigation: true,
        margin: 20,
        ltr: true,
        loop: true,
        dots: true,
        nav: false,
        //navText: ["<img src='owlCarousel/img/angle-left.png'>","<img src='owlCarousel/img/angle-right.png'>"],
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });


    $('#Testimonials_slide').owlCarousel({
        autoplay: true,
        autoplayTimeout: 8000,
        navigation: true,
        margin: 20,
        ltr: true,
        loop: true,
        dots: true,
        nav: false,
        //navText: ["<img src='owlCarousel/img/angle-left.png'>","<img src='owlCarousel/img/angle-right.png'>"],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    $('#spec1').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec2').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec3').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });

    $('#spec4').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec5').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec6').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });

    $('#spec7').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec8').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec9').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });

    $('#spec10').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec11').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec12').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });
    $('#spec13').click(function() {
        $('#specialization-modal')
            .prop('class', 'modal fade') // revert to default
            .addClass($(this).data('direction'));
        $('#specialization-modal').modal('show');
    });


});