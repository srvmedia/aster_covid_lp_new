<?php
//print_r($_SESSION);exit;
ob_start();
session_start();

//include("../dbconfig.php");
// is the one accessing this page logged in or not?

if ($_SESSION['logged-in'] != true || $_SESSION['logged-role'] != 'admin') {
// not logged in, move to login page
    header('Location: adminlogin.php');
//
exit;
} else {
    include("../dbconfig.php");

    $query = mysqli_query($conn,'SELECT * FROM enquiry_bpharma_temp order by id desc');
    $totaluser = mysqli_num_rows($query);
}
?>

<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mobile Admin | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->

        <!-- Theme style -->
        <link rel="stylesheet" href="css/AdminLTE.min.css">
        <link rel="stylesheet" href="js/dataTables/dataTables.bootstrap.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="css/skins/_all-skins.min.css">
        <style>
            .footer{
                padding: 15px 0 3px;
                text-align: center;
                background-color: #000;
                color: #fff;
                width:100%;
                
                bottom: 0;
            }
            @media (max-width: 414px){
    .content {
    padding: 15px 0!important;    
}
}
        </style>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">

        <header class="main-header">
            <!-- Logo -->
            <a href="index2.html" class="logo">

                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Admin</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">


                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <span class="hidden-xs">Welcome, Admin</span>
                            </a>

                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Content Wrapper. Contains page content -->
        <div class="container">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row">
                    <div class="col-sm-3">
                        <h1>
                           <a href="dashboard.php"> Dashboard</a>                           
                        </h1>
                    </div>
                    <div class="col-sm-3">
                    <h1>
                        <a href="email.php"> Email Leads</a> 
                        </h1>                      
                    </div>
                    <div class="col-sm-3">
                    <h1>
                        <a href="mobile.php"> Mobile Leads</a>
                        </h1>                       
                    </div>

                    <div class="col-sm-3">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                    </div>
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?php print_r($totaluser); ?></h3>

                                <p>Total Registrations</p>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="">
                            <div class="inner">
                                <a href="ExportExcel1.php"> <button type="button" class="btn btn-primary checklogin">Generate Excel Report</button></a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-lg-12 connectedSortable">

                        <div class="box box-primary">



                            <div class="table-responsive" style="padding:5px;"> 
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>

                                        <tr>
                                           
                                            <th>No<i class="fa fa-arrows-v" aria-hidden="true"></i></th>
                                            <th>Mobile<i class="fa fa-arrows-v" aria-hidden="true"></i></th> 
                                            <th>IP <i class="fa fa-arrows-v" aria-hidden="true"></i></th> 
                                            
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        
                                        $ip = $_SERVER['REMOTE_ADDR'];
                                        
                                        while ($row = mysqli_fetch_array($query)) {
                                            ?>

                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['mobile']; ?></td>       
                                               <td><?php echo  $row['ip'];  ?></td>
                                            </tr>

                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </section>

                </div>

            </section>
            <!-- /.content -->

            <!-- /.content-wrapper -->


        </div>
        <footer class="footer">
            <p class="mb-0">&copy; MITWPU 2020 | <span class="mb-0" style="font-size:13px;">Design &amp; Marketed By <a href="javascript:void(0);" style="color: #fff;">SRV Media Pvt. Ltd</a></span></p>
        </footer>

        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="js/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/dataTables/jquery.dataTables.js"></script>
        <script src="js/dataTables/dataTables.bootstrap.js"></script>
        <!-- Morris.js charts -->
        <!-- AdminLTE App -->
        <script src="js/app.min.js"></script>

    </body>
    <script>
        $(document).ready(function ()
        {
            $('table.dataTables-example').DataTable();
        });
    </script>

</html>
