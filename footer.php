<footer id="Contact" class="text-light">
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center py-4">
                    <p class="mb-3"><img src="img/globe (2).png" alt="icons" class="img-fluid mr-3"><a
                            href="www.asterlabs.in" target="_blank" class="text-white"> www.asterlabs.in</a></p>
                    <p class="mb-3"><img src="img/phone-call.png" alt="icons" class="img-fluid mr-3"><a
                            href="tel:080-4555 3333" target="_blank" class="text-white"> 080-4555 3333</a></p>
                    <p class="mb-3"><img src="img/mail.png" alt="icons" class="img-fluid mr-3"><a
                            href="mailto:customer.care@asterlabs.in" target="_blank" class="text-white">
                            customer.care@asterlabs.in</a></p>
                    <p>
                        <a href="https://www.facebook.com/AsterLabs.in/" target="_blank"><img src="img/social.png"
                                alt="icons" class="img-fluid mr-3"></a>
                        <a href="https://instagram.com/asterlabs.in" target="_blank"><img src="img/social-1.png"
                                alt="icons" class="img-fluid mr-3"></a>
                        <a href="https://www.youtube.com/channel/UCno3iuU-W1UAaa188dcBVgQ" target="_blank"><img
                                src="img/social-3.png" alt="icons" class="img-fluid mr-3"></a>
                    </p>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <p class="mb-0">© 2021 AsterLabs. All Rights Reserved. </p>
                        </div>
                        <div class="col-md-6 text-right">
                            <p class="mb-0">Designed &amp; Developed by <a href="https://www.srvmedia.com/"
                                    target="_blank" class="text-light">SRV Media Pvt. Ltd.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- <div class="container pt-5">
        <div class="row">
            <div class="col-md-3">
                <p>Sector 112, Landran, Greater Mohali, Punjab 140307 (INDIA)</p>
            </div>
            <div class="col-md-9">
                <div class="connect">
                    <p>Connect with us:</p>
                    <div class="f1">
                        <a href="#"><img src="images/facebook (1).png" alt="" class="img-fluid"></a>
                        <a href="#"><img src="images/linkedin (1).png" alt="" class="img-fluid"></a>
                        <a href="#"><img src="images/email.png" alt="" class="img-fluid"></a>
                        <a href="#"><img src="images/google-plus.png" alt="" class="img-fluid"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<!-- <hr>
    <p class="design mb-0">Designed by <a href="https://www.srvmedia.com/" class="text-white" target="_blank"
            rel="noopener noreferrer">
            SRV
            Media Pvt. Ltd.</a></p>
    </div> -->




<!--<div class="form-clickOuter"><span class="form-click opacityprimary open">Enquire Now</span></div>-->

<div class="clearfix"></div>

<script src="scripts/jquery.min.js"></script>
<script src="scripts/popper.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/wow.min.js"></script>
<script src="scripts/aos.js"></script>


<script src="build/js/intlTelInput.js"></script>
<!-- <script src="scripts/jquery-latest.min.js"></script> -->

<!-- <script src="scripts/jquery-ui.js"></script>
 -->
<script src="scripts/city.js"></script>


<script src="owlCarousel/js/owl.carousel.min.js"></script>
<script src="scripts/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="scripts/scripts.js"></script>
<!-- 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.4/awesomplete.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.4/awesomplete.js"></script>
 -->
<script type="text/javascript" src="code.js"></script>
<script type="text/javascript" src="utm_code.js"></script>


<script>
// var countryData = window.intlTelInputGlobals.getCountryData();
// var input = document.querySelector("#phone");
// var iti = window.intlTelInput(input, {
//     allowDropdown: true,
//     autoHideDialCode: false,
//     autoPlaceholder: "polite",
//     placeholderNumberType: "MOBILE",
//     separateDialCode: true,
//     initialCountry: "IN",
//     nationalMode: false,
//     dropdownContainer: document.body,
//     utilsScript: "build/js/utils.js",
// });

// var countryData_1 = window.intlTelInputGlobals.getCountryData();
// var input_1 = document.querySelector("#phone_1");
// var iti_1 = window.intlTelInput(input_1, {
//     allowDropdown: true,
//     autoHideDialCode: false,
//     autoPlaceholder: "polite",
//     placeholderNumberType: "MOBILE",
//     separateDialCode: true,
//     initialCountry: "IN",
//     nationalMode: false,
//     dropdownContainer: document.body,
//     utilsScript: "build/js/utils.js",
// });

// listen to the telephone input for changes
// input.addEventListener('countrychange', function(e) {
//     // console.log(iti.getSelectedCountryData().iso2);
//     $('#country_short_code').val(iti.getSelectedCountryData().iso2);
//     if (iti.getSelectedCountryData().iso2 != "in") {
//         $('#city_div').hide();
//         $('#cityhide1').show();
//     } else {
//         $('#city_div').show();
//         $('#cityhide1').hide();
//     }
// });

// // listen to the telephone input for changes
// input_1.addEventListener('countrychange', function(e) {
//     // console.log(iti.getSelectedCountryData().iso2);
//     $('#country_short_code_1').val(iti_1.getSelectedCountryData().iso2);
//     if (iti_1.getSelectedCountryData().iso2 != "in") {
//         $('#city_div_1').hide();
//         $('#cityhide_1').show();
//     } else {
//         $('#city_div_1').show();
//         $('#cityhide_1').hide();
//     }
// });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formMobile').hide();
    $('#formMobile1').hide();
});

$(document).ready(function() {
    // Add smooth scrolling to all links
    $(".navbar a, #download-btn, #download").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            // event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 80
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});
</script>
<script type="text/javascript">
var formdialog = true;
$(window).on('load', function() {
    $(document).mouseleave(function() {
        if (formdialog) {
            $('#myModal').modal('hide');
        }
        formdialog = false;
    });
    $(document).mouseenter(function() {});
    //show the scroll

    $('body').addClass('loaded');
});

$(document).ready(function() {
    $('#myModal').hide();


})
</script>
<script type="text/javascript">
var utm_source = '';
var utm_campaign = '';
var utm_medium = '';
var utm_link = '';
var utm = '';
$(document).ready(function() {
    var url = window.location.href;
    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // in case params look like: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function(v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // set parameter value (use 'true' if empty)
            var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // if parameter name already exists
            if (obj[paramName]) {
                // convert value to array (if still string)
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // if no array index number specified...
                if (typeof paramNum === 'undefined') {
                    // put the value on the end of the array
                    obj[paramName].push(paramValue);
                }
                // if array index number specified...
                else {
                    // put the value at that index number
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // if param name doesn't exist yet, set it
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    utm_source = obj['utm_source']
    utm_medium = obj['utm_medium']
    utm_campaign = obj['utm_campaign']
    if (typeof utm_campaign == 'undefined') {
        utm_campaign = '';
    }
    if (typeof utm_source == 'undefined') {
        utm_source = '';
    }
    if (typeof utm_medium == 'undefined') {
        utm_medium = '';
    }
});


// new start
var isMobile = false;
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i
    .test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
    .test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
}

if (isMobile == 1) {
    $('.dsu-form').css('right', '-310px');
    $('.mobile-banner').show();
    $('.dektop-banner').hide();
    var flag = 0;
} else {
    var flag = 1;
    $(window).scroll(function() {
        var height = $(window).scrollTop();
        if (height > 100) {
            $('.dsu-form').css('right', '-310px');
        } else {
            $('.dsu-form').css('right', '0px');
        }
    });
}

$(".form-clickOuter").on('click', function(e) {
    e.preventDefault();
    if (flag == 1) {
        $('.dsu-form').css('right', '-310px');
        flag = 0;
    } else {
        $('.dsu-form').css('right', '0px');
        flag = 1
    }
    return false;
});
// new end
</script>
<script type="text/javascript">
var s = document.createElement("script");
s.type = "text/javascript";
s.async = true;
s.src = "https://widgets.nopaperforms.com/emwgts.js";
document.body.appendChild(s);
</script>

<!-- lsquare -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.asterlabs.in"]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://lswebanalytics.com/analytics/";
    _paq.push(['setTrackerUrl', u+'lsquare.php']);
    _paq.push(['setSiteId', 231]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'lsquare.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<script type="text/javascript">
(function(d, src, c) { var t=d.scripts[d.scripts.length - 1],s=d.createElement('script');s.id='la_x2s6df8d';s.async=true;s.src=src;s.onload=s.onreadystatechange=function(){var rs=this.readyState;if(rs&&(rs!='complete')&&(rs!='loaded')){return;}c(this);};t.parentElement.insertBefore(s,t.nextSibling);})(document,
'//livesquare.in/livesq/scripts/track.js',
function(e){ LiveAgent.createButton('9e9dd0e5', e); });
</script>
<noscript><p><img src="http://lswebanalytics.com/analytics/lsquare.php?idsite=231" style="border:0;" alt="" /></p></noscript>
<!-- End lsquare Code -->