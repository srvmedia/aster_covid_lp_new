<!DOCTYPE html>
<html lang="en">

<head>
    <title>Aster Covid</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles/font-awesome.min.css">
    <link rel="stylesheet" href="styles/animate.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="owlCarousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="styles/aos.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">
    <link rel="stylesheet" href="build/css/demo.css">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="styles/responsive.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,800;1,400;1,600;1,800&display=swap"
        rel="stylesheet">

   <link href="https://d2p078bqz5urf7.cloudfront.net/jsapi/css/engagebay-source-forms-min.v01.css" rel="stylesheet">

    <link href="https://d2p078bqz5urf7.cloudfront.net/jsapi/css/engagebay-source-forms-min.v01.css" rel="stylesheet">
    <style>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap);

    .engage-bay-source-form.engagebay-forms[data-id="6071398964396032"] * {
        font-family: Open Sans
    }
    </style>
    <style>@import url(https://fonts.googleapis.com/css?family=Open+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap); .engage-bay-source-form.engagebay-forms[data-id="6071398964396032"] *{font-family:Open Sans}</style>
    <style>body{color: #333;}</style>
    <style>
    body {
        color: #333;
    }

    .health_package{
        background: url(img/bg1.jpg);
    background-size: cover;
    background-repeat: no-repeat;
    }
    .clr1 {
    color: #00B38D;
}
.acc{
    height: 81%;
}

 </style>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-KZCTJ9H');</script>

<script type="text/javascript" >
var EhAPI = EhAPI || {}; EhAPI.after_load = function(){
EhAPI.set_account('qf7b062muunchi0pgmuc3tlj44', 'asterlabs');
EhAPI.execute('rules');};(function(d,s,f) {
var sc=document.createElement(s);sc.type='text/javascript';
sc.async=true;sc.src=f;var m=document.getElementsByTagName(s)[0];
m.parentNode.insertBefore(sc,m);   
})(document, 'script', '//d2p078bqz5urf7.cloudfront.net/jsapi/ehform.js');
</script>

<!-- End Google Tag Manager -->
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="120">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZCTJ9H"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>



<!-- End Google Tag Manager (noscript) -->
    <?php include('header.php');?>



    <section class="banner-section position-relative" id="home">
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                <div class="col-md-12">
                    <img src="img/Banner-1.jpg" class="w-100 img-fluid d-md-block d-none">
                    <img src="img/bg1.jpg" class="d-md-none d-block w-100 img-fluid" alt="">
                </div>
            </div>
            <div class="row position-absolute">
                <div class="col-md-4 ml-auto text-white">
                    <h2 class="hc">Can’t afford a delay in your COVID-19 test reports?</h2>
                    <h3 class="mt-md-5 mt-3"><a href="#" class="button">BOOK WITH US</a></h3>
                    <p>Get Report Delivery in 24 Hrs</p>
                </div>
                <div class="col-md-4 d-md-block d-none">
                    <div class="banner_form wow bounceInDown">
                        <div class="banner-form">
                            <!-- <div class="form-title">
                                <h4 class=""> Enquire Now </h4>
                            </div> -->
                            <!-- <form name="register-form">
                                <div class="">
                                    <div class="form_div">
                                        <div id="allerror" class="font-weight-bold text-danger mb-2"></div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Name" id="name">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Email" id="email">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Mobile" id="mobile">
                                        </div>
                                        <div class="input-group mb-3">
                                            <select class="form-control" name="package_name" id="package_name">
                                                <option value="0" data-package="">Packages Name</option>
                                                <option value="1" data-package="package 1">package 1</option>
                                                <option value="2" data-package="package 2"> package 2
                                                </option>
                                                <option value="3" data-package="Package 3"> Package 3
                                                </option>
                                            </select>
                                        </div>
                                      
                                    </div>
                                    <div class="">
                                        <input class="btn text-light font-weight-bold text-uppercase w-100"
                                            id="enquiry_btn" type="button" value="Send">
                                    </div>
                                </div>
                            </form> -->


                            <!--  -->


                            <div class="engage-bay-source-form engagebay-forms" data-id="6071398964396032">
                                <form class="form form-style-form1 default   "
                                    onsubmit="window.EhForm.submit_form(event,this)"
                                    style="background-color:#ffffff;max-width:550px;background-position:0% 0%;box-shadow: none;true"
                                    data-id="6071398964396032">
                                    <fieldset>
                                        <!-- Form Name -->
                                        <h2 class="form-title" style="">
                                            <p style="text-align: left;" data-mce-style="text-align: left;">
                                                <strong>Enquiry Form</strong></p>
                                        </h2>
                                        <div class="form-group" style="">
                                            <div class="controls">
                                                <input data-ebay_field="name" data-ebay_add_as="" id="name" title=""
                                                    name="name" type="text" style="background-color:#fff;"
                                                    placeholder="Full Name" class="form-control" required="true">
                                            </div>
                                        </div>
                                        <div class="form-group" style="">
                                            <div class="controls">
                                                <input data-ebay_field="email" data-ebay_add_as="" id="email" title=""
                                                    name="email" type="email" style="background-color:#fff;"
                                                    placeholder="Email ID" class="form-control" required="true">
                                            </div>
                                        </div>
                                        <div class="form-group" style="">
                                            <div class="controls">
                                                <input data-ebay_field="phone" data-ebay_add_as="" id="phone_number"
                                                    title="Please enter valid phone number." name="phone_number"
                                                    type="phone" style="background-color:#fff;" placeholder="Mobile No."
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group" style="">
                                            <div class="controls">
                                                <select data-ebay_field="Package" data-ebay-add="" data-ebay_add_as=""
                                                    type="select" id="Package" name="Package"
                                                    style="background-color:#fff;" placeholder="Select Test or Package"
                                                    class="form-control" required="true">
                                                    <option value="">Select Test or Package</option>
                                                    <option value="COVID RT-PCR">COVID RT-PCR</option>
                                                    <option value="Antibody Test">Antibody Test</option>
                                                    <option value="Covid Immunity Package (Basic)">Covid Immunity
                                                        Package (Basic)</option>
                                                    <option value="Covid Immunity Package (Extended)">Covid Immunity
                                                        Package (Extended)</option>
                                                    <option value="Immunity Package">Immunity Package</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="controls">
                                                <input data-ebay_field="" data-ebay_add_as="ADDASTAG"
                                                    id="eb_temp_field_hidden_field" name="eb_temp_field_hidden_field"
                                                    type="hidden" value="Website" style="background-color:#fff;"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="controls">
                                                <input data-ebay_field="" data-ebay_add_as="ADDASTAG"
                                                    id="eb_temp_field_hidden_field_1"
                                                    name="eb_temp_field_hidden_field_1" type="hidden" value="Paid"
                                                    style="background-color:#fff;" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div>
                                                <button data-ebay_field="Package" data-ebay_add_as="ADDASTAG"
                                                    type="submit" class="submit-btn" style="color:#fff;background-color:#00b38d;">
                                                    <p>SEND</p>
                                                </button>
                                                <br>
                                                <span id="error-msg"></span>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="error-success-container"></div>
                                </form>

                            </div>
                        </div>
                        <!--  -->

                    </div>
                </div>
            </div>
        </div>
        <div class="row d-md-none d-block">
            <div class="col-md-4">
                <div class="banner_form wow bounceInDown" style="visibility: visible; animation-name: bounceInDown;">
                    <div class="banner-form">
                        <!-- <div class="form-title">
                                <h4 class=""> Enquire Now </h4>
                            </div>
                            <form name="register-form">
                                <div class="">
                                    <div class="form_div">
                                        <div id="allerror" class="font-weight-bold text-danger mb-2"></div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Name" id="name">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Email" id="email">
                                        </div>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Mobile" id="mobile">
                                        </div>
                                        <div class="input-group mb-3">
                                            <select class="form-control" name="package_name" id="package_name">
                                                <option value="0" data-package="">Packages Name</option>
                                                <option value="1" data-package="package 1">package 1</option>
                                                <option value="2" data-package="package 2"> package 2
                                                </option>
                                                <option value="3" data-package="Package 3"> Package 3
                                                </option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="">
                                        <input class="btn text-light font-weight-bold text-uppercase w-100"
                                            id="enquiry_btn" type="button" value="Send">
                                    </div>
                                </div>
                            </form> -->
                        <!--  -->
                        <div class="engage-bay-source-form engagebay-forms" data-id="6071398964396032">
                            <form class="form form-style-form1 default   "
                                onsubmit="window.EhForm.submit_form(event,this)"
                                style="background-color:#ffffff;max-width:550px;background-position:0% 0%;box-shadow: none;true"
                                data-id="6071398964396032">
                                <fieldset>
                                    <!-- Form Name -->
                                    <h2 class="form-title" style="">
                                        <p style="text-align: left;" data-mce-style="text-align: left;"><strong>Enquiry
                                                Form</strong></p>
                                    </h2>
                                    <div class="form-group" style="">
                                        <div class="controls">
                                            <input data-ebay_field="name" data-ebay_add_as="" id="name" title=""
                                                name="name" type="text" style="background-color:#fff;"
                                                placeholder="Full Name" class="form-control" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group" style="">
                                        <div class="controls">
                                            <input data-ebay_field="email" data-ebay_add_as="" id="email" title=""
                                                name="email" type="email" style="background-color:#fff;"
                                                placeholder="Email ID" class="form-control" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group" style="">
                                        <div class="controls">
                                            <input data-ebay_field="phone" data-ebay_add_as="" id="phone_number"
                                                title="Please enter valid phone number." name="phone_number"
                                                type="phone" style="background-color:#fff;" placeholder="Mobile No."
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group" style="">
                                        <div class="controls">
                                            <select data-ebay_field="Package" data-ebay-add="" data-ebay_add_as=""
                                                type="select" id="Package" name="Package" style="background-color:#fff;"
                                                placeholder="Select Test or Package" class="form-control"
                                                required="true">
                                                <option value="">Select Test or Package</option>
                                                <option value="COVID RT-PCR">COVID RT-PCR</option>
                                                <option value="Antibody Test">Antibody Test</option>
                                                <option value="Covid Immunity Package (Basic)">Covid Immunity Package
                                                    (Basic)</option>
                                                <option value="Covid Immunity Package (Extended)">Covid Immunity Package
                                                    (Extended)</option>
                                                <option value="Immunity Package">Immunity Package</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input data-ebay_field="" data-ebay_add_as="ADDASTAG"
                                                id="eb_temp_field_hidden_field" name="eb_temp_field_hidden_field"
                                                type="hidden" value="Website" style="background-color:#fff;"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="controls">
                                            <input data-ebay_field="" data-ebay_add_as="ADDASTAG"
                                                id="eb_temp_field_hidden_field_1" name="eb_temp_field_hidden_field_1"
                                                type="hidden" value="Paid" style="background-color:#fff;"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <button data-ebay_field="Package" data-ebay_add_as="ADDASTAG" type="submit"
                                                class="submit-btn" style="color:#fff;background-color:#00b38d;
    ">
                                                <p>SEND</p>
                                            </button>
                                            <br>
                                            <span id="error-msg"></span>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="error-success-container"></div>
                            </form>

                        </div>
                    </div>
                    <!--  -->

                    <!--  -->
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <!-- section 2 -->

    <div class="bg1">
        <section class="covid">
            <div class="container">
                <div class="box1">
                    <div class="row pt-5">
                        <div class="col-md-3">
                            <img alt="" src="img/1.png" class="img-fluid f1" />
                        </div>
                        <div class="col-md-9">
                            <h1 class="text_blue">COVID RT-PCR Test <span class="text_green">@ 800/-*</span></h1>

                            <ul class="ul">
                                <li>Detects the presence of SARS-CoV-2 virus</li>
                                <li>Sample taken using Nasal or throat swab</li>
                            </ul>
                            <div class="row justify-content-center">
                                <div class="col-md-4 text-center col-6">
                                    <img alt="" src="img/1.1.png" class="img-fluid" />
                                    <p class="p1">Safety protocol<br> followed</p>
                                </div>
                                <div class="col-md-4 text-center col-6">
                                    <img alt="" src="img/1.2.png" class="img-fluid" />
                                    <p class="p1">Flexible <br>booking</p>
                                </div>
                                <div class="col-md-4 text-center col-6">
                                    <img alt="" src="img/1.3.png" class="img-fluid" />
                                    <p class="p1">Home sample <br>collection</p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <a href="#" class="bb btn_sec">BOOK NOW</a>

                                </div>
                                <div class="col-md-12">
                                    <p class="pl-5 mt-3">*Home visit charges applicable</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="test pb-5">
            <div class="container">
                <div class="box2">
                    <div class="row">
                        <div class="col-md-9 order-md-1 order-2">
                            <h1 class="antibody text_blue">COVID Antibody Test</h1>
                            <ul class="ul">
                                <li>Identifies IgG or IgM antibodies to SARS-CoV-2 virus</li>
                                <li>Detects antibody reaction to past COVID-19 infection</li>
                            </ul>
                            <div class="row justify-content-center">
                                <div class="col-md-4 h-100 col-6">
                                    <a href="#" class="box d-block text-dark">
                                        <h6 class="h1">COVID-19 (SARS-CoV-2) ANTIBODY IgM TEST</h6>
                                        <p class="text-green">₹ 500*</p>
                                    </a>
                                </div>

                                <div class="col-md-4 h-100 col-6">
                                    <a href="#" class="box d-block text-dark">
                                        <h6 class="h1">COVID-19 (SARS-CoV-2) ANTIBODY IgG
                                            TEST</h6>
                                        <p class="text-green">₹ 500*</p>
                                    </a>
                                </div>

                                <div class="col-md-4 h-100 col-6">
                                    <a href="#" class="box d-block text-dark">
                                        <h6 class="h1">COVID-19 (SARS-CoV-2) ANTIBODY TOTAL
                                            (IgG) TEST
                                        </h6>
                                        <p class="text-green">₹ 850*</p>
                                    </a>
                                </div>
                            </div>
                            <div class="row pt-5 text-sm-center justify-content-center">
                                <div class="col-md-4 col-6 text-center">
                                    <img alt="" src="img/1.1.png" class="img-fluid" />
                                    <p class="p1">Safety protocol<br> followed</p>
                                </div>
                                <div class="col-md-4 col-6">
                                    <img alt="" src="img/1.4.png" class="img-fluid" />
                                    <p class="p1">Non-fasting<br> blood test</p>
                                </div>
                                <div class="col-md-4 col-6">
                                    <img alt="" src="img/1.3.png" class="img-fluid" />
                                    <p class="p1">Home sample <br>collection</p>
                                </div>

                            </div>
                            <div class="row justify-content-center text-sm-left text-center">
                                <div class="col-md-12 mt-4">
                                    <a href="#" class="bb btn_sec">BOOK NOW</a>
                                    <p class="mt-3">*Home visit charges applicable</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 order-md-2 order-1">
                            <img src="img/2.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
        </section>

    </div>

    <section class=" health_package">
        <div class="container ">
            <div class="row pt-5 pb-5">
                <div class="col-md-10 mx-auto">
                    <div class="row justify-content-between">
                        <div class="col-md-12 text-center">
                            <h1 class="covid text-white">Covid-19 Cure Package</h1>
                        </div>
        

                                <div class="col-md-4 mb-4">
                    <div class="box" style="height: 100% !important">
                        <div class="">
                            <strong><p class="mb-0 text-center">Covid-19 Cure - Basic</p></strong>
                          
                        </div>
                        <div class="box_content acc">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4 text-center"><span class="clr1 ">₹999</span></h4>
                            <p class="text-center">CBC</p>
                            <p class="text-center">SGOT</p>
                            <p class="text-center">SGPT</p>
                            <p class="text-center">Serum Creatinine</p>
                            <p class="text-center">IgG Specific Antibodies</p>
                            <p class="text-center">Glycosylated Haemoglobin (HbA1c)</p>
                            <p class="text-center">CPK</p>
                        </div>
                         <div class="col-md-12 text-center mt-3">
                            <a href="#" class="bb btn_sec">BOOK NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="box" style="height: 100% !important">
                        <div class="">
                           <strong><p class="mb-0 text-center">Covid-19 Cure - Extended</p></strong>
                        </div>
                        <div class="box_content" style="height:81%">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4 text-center"> <span class="clr1 ">₹1699</span></h4>
                            <p class="text-center">CBC</p>
                            <p class="text-center">ESR</p>
                            <p class="text-center">CRP</p>
                            <p class="text-center">SGOT</p>
                            <p class="text-center">SGPT</p>
                            <p class="text-center">Proteins (Albumin)</p>
                            <p class="text-center">Serum Creatinine</p>
                            <p class="text-center">IgG Specific Antibodies</p>
                            <p class="text-center">Glycosylated Haemoglobin (HbA1c)</p>
                            <p class="text-center">CPK</p>
                            <p class="text-center">LDH</p>
                            <p class="text-center">FERRITIN</p>
                        </div>
                           <div class="col-md-12 text-center mt-3">
                            <a href="#" class="bb btn_sec">BOOK NOW</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-4">
                    <div class="box" style="height: 100% !important">
                        <div class="">
                            <strong><p class="mb-0 text-center">Covid-19 Cure - Advance</p></strong>
                        </div>
                        <div class="box_content" >
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4 text-center"><span class="clr1 ">₹2499</span></h4>
                             <p class="text-center">CBC</p>
                            <p class="text-center"> ESR</p>
                            <p class="text-center">CRP</p>
                            <p class="text-center">SGOT</p>
                            <p class="text-center">SGPT</p>
                            <p class="text-center">Proteins (Albumin)</p>
                            <p class="text-center">Serum Creatinine</p>
                            <p class="text-center">IgG Specific Antibodies</p>
                            <p class="text-center">Glycosylated Haemoglobin (HbA1c)</p>
                            <p class="text-center">D -Dimer</p>
                            <p class="text-center">CPK</p>
                            <p class="text-center">LDH</p>
                            <p class="text-center">FERRITIN</p>
                        </div>
                        <div class="col-md-12 text-center mt-3">
                            <a href="#" class="bb btn_sec">BOOK NOW</a>
                        </div>
                    </div>
                </div>
                            <!-- <div class="table-responsive mb-5"> -->
                                <!-- <table class="table table-bordered1 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="th" width="30%">Test Name</th>
                                            <th class="th" width="15%">Price</th>
                                            <th class="th" width="55%">Test Component</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>Covid Immunity package basic</td>
                                            <td class="td">₹ 999</td>
                                            <td>
                                                <p>Covid IgM, Covid IgG, CBC, Total Protein with Albumin A/G</p>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>Covid Immunity package extended</td>
                                            <td class="td">₹ 1999</td>
                                            <td>
                                                <p>Covid IgM, Covid IgG, CBC, Total Protein with Albumin A/G,
                                                    Creatinine, SGPT,
                                                    LDH,
                                                    CK
                                                    -
                                                    MB</p>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>Immunity package</td>
                                            <td class="td">₹ 2399</td>
                                            <td>
                                                <p>Covid IgG, CBC, Immunoglobulin Profile - Total IgG, Total IgM, Total
                                                    IgA;
                                                    Total
                                                    Protein
                                                    Albumin A/G, Vitamin D</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> -->
                        <!--         <table class="table table-bordered1 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="th" width="30%">Covid 19 Cure - Basic</th>
                                            <th class="th" width="15%" style="text-align:center; vertical-align: middle !important;">MRP</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>CBC</td>
                                            <td rowspan="7" style="text-align:center; vertical-align: middle !important;" class="td">₹ 999</td>
                                           
                                        </tr>
                                        <tr>

                                            <td>SGOT</td>
                                        
                                          
                                        </tr>
                                        <tr>

                                            <td>SGPT</td>
                                           
                                         
                                        </tr>
                                         <tr>

                                            <td>Serum Creatinine</td>
                                         
                                         
                                        </tr>
                                         <tr>

                                            <td>IgG Specific Antibodies</td>
                                            
                                         
                                        </tr>
                                         <tr>

                                            <td>Glycosylated Haemoglobin (HbA1c)</td>
                                    
                                         
                                        </tr>
                                          <tr>

                                            <td>CPK</td>
                                            
                                         
                                        </tr>
                                    </tbody>
                                </table>

                                <br> -->
<!-- 
                                <table class="table table-bordered1 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="th" width="30%">Covid 19 Cure - Extended</th>
                                            <th class="th" width="15%" style="text-align:center; vertical-align: middle !important;">MRP</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>CBC</td>
                                            <td rowspan="12" style="text-align:center; vertical-align: middle !important;" class="td">₹ 1699</td>
                                           
                                        </tr>
                                        <tr>

                                            <td>ESR</td>
                                        
                                          
                                        </tr>
                                        <tr>

                                            <td>CRP</td>
                                           
                                         
                                        </tr>
                                         <tr>

                                            <td>SGOT</td>
                                         
                                         
                                        </tr>
                                         <tr>

                                            <td>SGPT</td>
                                            
                                         
                                        </tr>
                                         <tr>

                                            <td>Proteins (Albumin)</td>
                                    
                                         
                                        </tr>
                                          <tr>

                                            <td>Serum Creatinine</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>IgG Specific Antibodies</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>Glycosylated Haemoglobin (HbA1c)</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>CPK</td>
                                            
                                         
                                        </tr>
                                        <tr>

                                            <td>LDH</td>
                                            
                                         
                                        </tr>
                                        <tr>

                                            <td>FERRITIN</td>
                                            
                                         
                                        </tr>
                                    </tbody>
                                </table>

                                <br> -->

                                <!--  <table class="table table-bordered1 table-striped">
                                    <thead>
                                        <tr>
                                            <th class="th" width="30%">Covid 19 Cure - Advance</th>
                                            <th class="th" width="15%" style="text-align:center; vertical-align: middle !important;">MRP</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>CBC</td>
                                            <td rowspan="13" style="text-align:center; vertical-align: middle !important;" class="td">₹ 2499</td>
                                           
                                        </tr>
                                        <tr>

                                            <td>ESR</td>
                                        
                                          
                                        </tr>
                                        <tr>

                                            <td>CRP</td>
                                           
                                         
                                        </tr>
                                         <tr>

                                            <td>SGOT</td>
                                         
                                         
                                        </tr>
                                         <tr>

                                            <td>SGPT</td>
                                            
                                         
                                        </tr>
                                         <tr>

                                            <td>Proteins (Albumin)</td>
                                    
                                         
                                        </tr>
                                          <tr>

                                            <td>Serum Creatinine</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>IgG Specific Antibodies</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>Glycosylated Haemoglobin (HbA1c)</td>
                                            
                                         
                                        </tr>
                                        <tr>

                                            <td>D -Dimer</td>
                                            
                                         
                                        </tr>

                                        <tr>

                                            <td>CPK</td>
                                            
                                         
                                        </tr>
                                        <tr>

                                            <td>LDH</td>
                                            
                                         
                                        </tr>
                                        <tr>

                                            <td>FERRITIN</td>
                                            
                                         
                                        </tr>
                                    </tbody>
                                </table> -->
                            <!-- </div> -->


                       <!--  <div class="col-md-12 text-center">
                            <a href="#" class="bb btn_sec">BOOK NOW</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>


    </section>

        <!--====== Certified Diagnostic Center. Reasonable Rates. end ==========-->

    <!--====== Health Packages start ==========-->
 <!--    <section class="health_package  py-md-5 py-3" id="health_package">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title1 text-center text-white wow fadeInUp  animated" data-wow-duration="1.3s">Health
                        Packages</h2>
                        <p class="text-center text-white">We offer more than 2500 tests. Here are our popular ones</p>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-3 mb-4">
                    <div class="box">
                        <div class="">
                            <p><img src="images/health_package/1.png" alt="Health Packages" class="img-fluid"></p>
                            <p class="mb-0">(No. of Tests - 8)</p>
                        </div>
                        <div class="box_content">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4"><s>₹1390</s><span class="clr1"> ₹699</span></h4>
                            <p>Glucose, Fasting (F)</p>
                            <p>Urine Examination, Routine</p>
                            <p>Cholesterol, Total</p>
                            <p>Triglycerides, Serum</p>
                            <p>SGOT, Aspartate Amino Transferase (AST)</p>
                            <p>SGPT, Alanine Amino Transferase (ALT)</p>
                            <p>Creatinine, Serum</p>
                            <p>Glycosylated Haemoglobin (HBA1C)</p>
                        </div>
                        <div class="btn_box">
                            <p><a href="#" class="btn btn_pri">Enquire Now</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="box">
                        <div class="">
                            <p><img src="images/health_package/2.png" alt="Health Packages" class="img-fluid"></p>
                            <p class="mb-0">(No. of Tests - 46)</p>
                        </div>
                        <div class="box_content">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4"><s>₹2170</s> <span class="clr1">₹999</span></h4>
                            <p>Glucose, Fasting (F)</p>
                            <p>Thyroid Profile, Total</p>
                            <p>Lipid Profile, Basic</p>
                            <p>LFT Liver Function Test</p>
                            <p>KFT Kidney Panel</p>
                            <p>Urine Examination, Routine</p>
                        </div>
                        <div class="btn_box">
                            <p><a href="#" class="btn btn_pri">Enquire Now</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="box">
                        <div class="">
                            <p><img src="images/health_package/3.png" alt="Health Packages" class="img-fluid"></p>
                            <p class="mb-0">(No. of Tests - 71)</p>
                        </div>
                        <div class="box_content">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4"><s>₹2870</s> <span class="clr1">₹1299</span></h4>
                            <p>Glucose, Fasting (F)</p>
                            <p>Thyroid Profile, Total</p>
                            <p>Lipid Profile, Basic</p>
                            <p>LFT Liver Function Test</p>
                            <p>KFT Kidney Panel</p>
                            <p>Urine Examination, Routine</p>
                            <p>CBC Complete Blood Count</p>
                            <p>Glycosylated Haemoglobin (HBA1C)</p>
                        </div>
                        <div class="btn_box">
                            <p><a href="#" class="btn btn_pri">Enquire Now</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-4">
                    <div class="box">
                        <div class="">
                            <p><img src="images/health_package/4.png" alt="Health Packages" class="img-fluid"></p>
                            <p class="mb-0">(No. of Tests - 73)</p>
                        </div>
                        <div class="box_content">
                            <hr class="line" style="height:3px; margin: 10px -15px 15px; ">
                            <h4 class="mb-4"><s>₹4470</s> <span class="clr1">₹2299</span></h4>
                            <p>Glucose, Fasting (F)</p>
                            <p>Thyroid Profile, Total</p>
                            <p>Lipid Profile, Basic</p>
                            <p>LFT Liver Function Test</p>
                            <p>KFT Kidney Panel</p>
                            <p>Urine Examination, Routine</p>
                            <p>Glycosylated Haemoglobin (HBA1C)</p>
                            <p>Vitamin B12 Cyanocobalamin</p>
                            <p>Vitamin D 25- Hydroxy</p>
                        </div>
                        <div class="btn_box">
                            <p><a href="#" class="btn btn_pri">Enquire Now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--====== Health Packages end ==========-->

    <?php include('footer.php');?>


</body>

</html>
