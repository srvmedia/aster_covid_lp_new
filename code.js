/*  new Awesomplete('input[type="email"]', {
    list: ['gmail.com','yahoo.com','hotmail.com','aol.com','msn.com','yahoo.co.in','live.com',
    'rediffmail.com','ymail.com','outlook.com', 'yahoo.in', 'mail.com'],
    data: function (text, input) {
      return input.slice(0, input.indexOf("@")) + "@" + text;
    },
    filter: Awesomplete.FILTER_STARTSWITH,
    sort: false
  });
*/

$("#submit_btn").click(function () {   

    // alert("hello");

    var name = $.trim($("#name").val());    
    var number = $.trim($("#phone").val());
    var email = $.trim($("#email").val());    
    var city = "";
    var course = $.trim($('#course').val()); 
    var ip = $('#storeIP').val();
    
    var pcode= $('.banner-form .iti__selected-dial-code').text();

    //alert(pcode);

    var programmes = $.trim($("#programmes").val());

    var mob = /^[0-9]{10}$/;

    custom_link();
	
    if (!name)
    {
        $('#errortop').html("Enter Your Full Name.");
        return false;
    }
    if (/[^a-zA-Z \-]/.test(name))
    {
        $('#errortop').html("Enter only alphabets in name.");
        return false;
    }

    if (!email)
    {
        $('#errortop').html("Enter your Email ID.");
        return false;
    }

    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    var valid = emailReg.test(email);
    if (!valid) {
        $('#errortop').html("Enter Valid Email ID.");
        return false;
    }
    if (!number)
    {
        $('#errortop').html("Enter your mobile number.");
        return false;                   }
    if (mob.test(number) == false) {
        $('#errortop').html("Enter 10 digit mobile Number.");
        return false;
    }
   
    number = pcode+"-"+number;

    // alert(number);

    if(pcode=="+91")
    {
        city =$.trim($('#cityh').val());
    }
    else 
    {
        city =$.trim($('#city').val());
    }
    if (!city)
    {
        $('#errortop').html("Enter your District.");
        return false;
    }
    if (/[^a-zA-Z1-9 \-()]/.test(city))
    {
        $('#errortop').html("Enter your District in alphabets.");
        return false;
    }
   
    if (!programmes)
    {
        $('#errortop').html("Select programme");
        return false;
    }
    var utm= "?utm_source="+utm_source+"&utm_medium="+utm_medium+"&utm_campaign="+utm_campaign;   
  
    /*$.ajax({

        url: "https://script.google.com/macros/s/AKfycbwMvJwYi6uubcrEDSyoBgHch7bAbLlndQbW8YCzlNJWPEq08KOm/exec",
        dataType : 'json',
        data: {'First Name':name,'Phone No.':pcode+"-"+number,'Email':email,'City':city,'Course':course,'Source':utm_source,'Medium':utm_medium},
        type: 'GET',
        success: function(response)
        { }
    });*/

    document.getElementById('submit_btn').style.visibility = 'hidden';
    $('#errortop').html("Please wait for a while.");
    $.ajax({
        
        url: "submitenquiry.php",
        dataType : 'json',
        data: {id:"main_form",name:name,number:number,programmes:programmes,email:email,city:city,course:course,
        utm_source:utm_source,utm_medium:utm_medium,utm_campaign:utm_campaign,ip:ip,countrycode: $('#country_short_code').val()},
        type: 'POST',
        success: function(response)
        {
            console.log(response);
            if(response.status=="success")
            {   
               
            var fname=name.split(' ');
            var first_name='';
            var last_name='';
            if(fname[0]!=''){
            first_name=fname[0];
            }
            if(fname[1]!=''){
            last_name=fname[1];
            }
            if (fname.length==1){
            last_name='null';			
            }

            if(utm_source==''){ utm_source="SRV"}
            if(utm_medium==''){ utm_medium="Digital"}
            if(utm_campaign==''){utm_campaign="(not set)"}
            if((utm_source=='')&&(utm_medium==''))
            {
            utm_campaign="(not set)"
            }
            if((utm_campaign=='')&&(utm_medium==''))
            {
            utm_source="SRV"
            }
            if((utm_source=='')&&(utm_campaign==''))
            {
            utm_medium="Digital"
            }
            if((utm_source=='')&&(utm_campaign=='')&&(utm_medium==''))
            {
            utm_medium="Digital";
            utm_source="SRV"
            utm_campaign="(not set)"			
            }

            window.location.href="thankyou.php"+utm;

            }
            else{
                alert("Please Try later");
            }
        }
    });

});


$("#enquire_btn2").click(function () {
var name = $.trim($("#name1").val());
var email = $.trim($("#email1").val()); 
var number = $.trim($("#phone_1").val()); 
var city = "";
var course = $.trim($('#course1').val()); 

var ip = $('#storeIP1').val();

var pcode1= $('.form-modal .iti__selected-dial-code').text();

//alert(pcode1);

var programmes = $.trim($("#programmes1").val());

var mob = /^[0-9]{10}$/;

if (!name)
{
    $('#errortopm').html("Enter Your Full Name.");
    return false;
}
if (/[^a-zA-Z \-]/.test(name))
{
    $('#errortopm').html("Enter only alphabets.");
    return false;
} 
if (!email)
{
    $('#errortopm').html("Enter your Email ID.");
    return false;
}
var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
var valid = emailReg.test(email);
if (!valid) {
    $('#errortopm').html("Enter Valid Email ID.");
    return false;
}
if (!number)
{
    $('#errortopm').html("Enter your mobile number.");
    return false;                   
}
if (mob.test(number) == false) {
    $('#errortopm').html("Enter 10 digit mobile Number.");
    return false;
}
number = pcode1+"-"+number;

//alert(number);

if(pcode1=="+91")
{
    city =$.trim($('#cityh_1').val());
}
else 
{
    city =$.trim($('#city_1').val());
}
if (!city)
{
    $('#errortopm').html("Enter your District.");
    return false;
}
if (/[^a-zA-Z1-9 \-()]/.test(city))
{
    $('#errortopm').html("Enter your District in alphabets.");
    return false;
}

if (!programmes)
{
    $('#errortopm').html("Select programme");
    return false;
}

custom_link();

var utm= "?utm_source="+utm_source+"&utm_medium="+utm_medium+"&utm_campaign="+utm_campaign

document.getElementById('enquire_btn2').style.visibility = 'hidden';

$('#errortopm').html("Please wait for a while.");

$.ajax({
  url: "submitenquiry.php", 
  dataType : 'json',
  data: {id:"modal_enquiry",name:name,number:number,programmes:programmes,email:email,city:city,course:course,utm_source:utm_source,utm_medium:utm_medium,utm_campaign:utm_campaign,ip:ip,countrycode: $('#country_short_code_1').val()},
  type: 'POST',
  success: function(response)
  {
    console.log(response);
    if(response.status=="success")
    {
        var fname=name.split(' ');
        var first_name='';
        var last_name='';
        if(fname[0]!=''){
        first_name=fname[0];
        }
        if(fname[1]!=''){
        last_name=fname[1];
        }
        if (fname.length==1){
        last_name='null';			
        }
        if(utm_source==''){ utm_source="SRV"}
        if(utm_medium==''){ utm_medium="Digital"}
        if(utm_campaign==''){utm_campaign="(not set)"}
        if((utm_source=='')&&(utm_medium==''))
        {
        utm_campaign="(not set)"
        }
        if((utm_campaign=='')&&(utm_medium==''))
        {
        utm_source="SRV"
        }
        if((utm_source=='')&&(utm_campaign==''))
        {
        utm_medium="Digital"
        }
        if((utm_source=='')&&(utm_campaign=='')&&(utm_medium==''))
        {
        utm_medium="Digital";
        utm_source="SRV"
        utm_campaign="(not set)"
        }
        
        var a = $("<a>").attr("href", "MSc_Chemistry_Brochure.pdf").attr("download", "MSc_Chemistry_Brochure.pdf").appendTo("body");
        a[0].click();
        a.remove();
        jQuery('.close').trigger('onclick');
                   
        window.location.href="thankyou.php"+utm;
    }
    else{
        alert("Please Try later");
    }
  }
});
});



$("#phone").blur(function () {
console.log('phone');
var email = $.trim($("#email").val());
var number = $.trim($("#phone").val()); 
var pcode= $('.banner-form .iti__selected-dial-code').text();

number = pcode+"-"+number;   
        
$.ajax({
url: "leads_temp.php", 
dataType : 'json',
data: {email:email,number:number},
type: 'POST',
success: function(response)
{ console.log(response); }
});

});

$("#email").blur(function () { 
console.log('email');
var email = $.trim($("#email").val());
var number = $.trim($("#phone").val());    
var pcode= $('.banner-form .iti__selected-dial-code').text();

number = pcode+"-"+number;   

$.ajax({
url: "leads_temp.php", 
dataType : 'json',
data: {email:email,number:number},
type: 'POST',
success: function(response)
{console.log(response); }
});

});

function openModal(title,content){
    $('#modaltitle').html(title);
    $('#modalcontent').html(content);
}


