<header>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mx-auto text-center">
                        <a class="navbar-brand" href="#home">
                            <img alt="logo" src="img/Aster lab logo.png" class="img-fluid" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="clearfix"></div>
        <div class="w-100"> -->
        <!-- <a class="navbar-brand pl-2 d-lg-none d-inline-block" href="#home">
                <img alt="logo" src="images/logo.png" class="img-fluid" />
            </a> -->
        <!-- Toggler/collapsibe Button -->
        <!-- <button class="navbar-toggler float-right mt-3" type="button" data-toggle="collapse"
                data-target="#navbarResponsive">
                <span class="navbar-toggler-icon"></span>
            </button> -->
        <!-- Navbar links -->
        <!-- <div class="collapse navbar-collapse w-100 py-1" id="navbarResponsive">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#home">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#COURSES">COURSES OFFERED</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ADVANTAGES">CGC ADVANTAGES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#INTERNATIONAL">INTERNATIONAL EDGE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#PLACEMENT">PLACEMENT</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#TESTIMONIALS">TESTIMONIALS</a>
                    </li>
                </ul>
            </div>
        </div> -->
    </nav>

</header>

<!--<div class="pointer1">
    <div class="">
        <a href="#" class="vertical text-uppercase">Enquire Now</a>
    </div>
</div>-->

<!--<div class="pointer11" id="enquirysection">
    <div class="">
        <h4 class="p-2 text-white mb-0" style="background: #27426D;">Get More Info</h4>
        <div class="npf_wgts" data-height="420px" data-w="470640b80a76d43676870368ec54f23b"></div>
    </div>
    
</div>-->